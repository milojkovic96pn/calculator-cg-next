
export default function AboutUs() {

    return (
        <main className="bg-[#fff9f9]">
            <div className={"p-[24px] bg-[#fff9f9] w-full mx-auto text-black container"}>
                <br/>
                <h1 className={"text-black text-[25px]"}>
                    O nama
                </h1> <br/>
                <p className={"text-[23px]"}>
                    Mi smo vrhunski tim stručnjaka koji se bavi mašinskim malterisanjem. Naša kompanija ima višegodišnje iskustvo u
                    ovoj oblasti i ponosimo se pružanjem kvalitetnih usluga našim klijentima. <br/><br/>
                    Naš cili je da Vam pružimo efikasno i profesionalno mašinsko malterisanje koje će ispuniti sve Vaše potrebe i očekivanja. <br/>
                    Naš tim iskusnih majstora koristi najnoviju opremu i tehnologiju kako bi osigurao visok nivo preciznosti i kvaliteta u svakom projektu. <br/><br/>
                    Sa nama, možete biti sigurni da će Vaši zidovi biti savršeno izmalterisani u kratkom vremenskom roku. Naša sposobnost da brzo i tačno nanesemo
                    malter na poršine omogućava Vam da uštedite vreme i energiju. <br/><br/>
                    Pored toga, cijenimo važnost dobrog odnosa sa našim klijentima. Naš tim je posvećen pružanju izuzetne korisničke
                    podrške i radimo zajedno sa Vama kako bismo postigli rezultate koji su u skladu sa Vašim željama i zahtevima.
                </p> <br/>
            </div>
        </main>
    )
}