'use client'
import {useForm} from "react-hook-form";
import {InputField} from "@/app/components/inputs/InputField";
import {yupResolver} from "@hookform/resolvers/yup";
import * as Yup from "yup";
import {CheckboxField} from "@/app/components/inputs/CheckboxField";
import {useState} from "react";
import "./components/utils/icons";
import {SelectDropdown} from "@/app/components/inputs/SelectDropdown";
import {TextArea} from "@/app/components/inputs/TextArea";
import {Button} from "@/app/components/Button";
import emailjs from '@emailjs/browser';
import toast from "react-hot-toast";

export default function Home() {

    const [loading,setLoading] = useState(false);
    const validation = Yup.object().shape({
        name: Yup.string().required("* obavezno polje"),
        address: Yup.string().required("* obavezno polje"),
        phone: Yup.string().required("* obavezno polje"),
    });
    const {
        register,
        handleSubmit,
        formState: { errors },
        control,
        watch,
        reset
    } = useForm<any>({
        resolver: yupResolver(validation),
    });

    const surfaceWatch = watch("surface")
    const townWatch = watch("town")

    const [input, setInput] = useState("");

    const handleChangeCheckbox = (field:string) => {
       if (input === field) {
           setInput("")
       } else {
           setInput(field)
       }
    }

    const handleCalculate = () => {
        const price = input === "yes" ? (townWatch === undefined || townWatch === "niksic" ? 11 : 12) : (townWatch === undefined || townWatch === "niksic" ? 6 : 7)
        return Number(surfaceWatch) * price || 0
    }

    const onSubmit = async (data:any) => {
        setLoading(true)
        const serviceId = 'service_mhygzvd'
        const templateId = 'template_xx5g3zq'
        const publicKey = 'SKBV-qp_H16amWI82'

        var templateParams = {
            name: data.name,
            email: data.email,
            phone: data.phone,
            address: data.address,
        };

        emailjs.send(serviceId, templateId, templateParams, publicKey).then(
            (response) => {
                setLoading(false)
                reset()
                toast.success("Uspešno poslat zahtev!")
            },
            (error) => {
                setLoading(false)
                toast.error("Greška. Pokušajte kasnije!")
            },
        );

    };

  return (
    <main className="min-h-screen bg-[#fff9f9]">
        <div className={"flex flex-col gap-5"}>
            <div className={"w-full"}>
                <div className={""}>
                    <div className={"container m-auto p-[24px] lg:flex justify-center items-center gap-5 text-center"}>
                        <h1 className={"text-[25px] text-black border-b border-mainColor mb-5 pb-4 lg:border-0 lg:mb-0 lg:pb-0"}>
                            Kalkulator cijene
                        </h1>
                        <div className={"border-2 border-mainColor h-[55px] w-[2px] hidden lg:block"}>
                        </div>
                        <div className={"text-black text-[19px]"}>
                            <p>
                                Naruči mašinsko malterisanje putem interneta
                            </p>
                            <p>
                                Proveri cijenu putem kalkulatora
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div className={"lg:grid grid-cols-2 container m-auto p-[24px]"}>
                <div className={"flex justify-center items-center lg:w-[500px] m-auto "}>
                    <div className={"flex flex-col justify-start items-start gap-5 pt-[20px] w-full"}>
                        <InputField
                            labelName={"Površina zidova i plafona"}
                            registration={register("surface")}
                            error={errors?.surface?.message || null}
                            inputType="text"
                            extraField={
                                <span className={"text-black"}>
                                m2
                              </span>
                            }
                            type={"number"}
                            classNameContainer={"lg:w-[250px]"}
                        />

                        <div className={"flex flex-col gap-5 justify-start items-start"}>
                            <label className="text-[19px] text-black">
                                Da li da izvodjač obezbijedi materijal?
                            </label>
                            <CheckboxField
                                onChange={() => handleChangeCheckbox("yes")}
                                selected={input === "yes"}
                                label={"Da, želim"}
                            />
                            <CheckboxField
                                onChange={() => handleChangeCheckbox("no")}
                                selected={input === "no"}
                                label={"Ne, ja ću osigurati materijal"}
                            />
                        </div>
                        <SelectDropdown
                            label={"Grad"}
                            options={
                                [
                                    {
                                        label: "Podgorica",
                                        value: "podgorica",
                                    },
                                    {
                                        label: "Budva",
                                        value: "budva"
                                    },
                                    {
                                        label: "Kotor",
                                        value: "kotor"
                                    },
                                    {
                                        label: "Tivat",
                                        value: "tivat"
                                    },
                                    {
                                        label: "Bar",
                                        value: "bar"
                                    },
                                    {
                                        label: "Ulcinj",
                                        value: "ulcinj"
                                    },
                                    {
                                        label: "Nikšić",
                                        value: "niksic"
                                    },
                                    {
                                        label: "Herceg Novi",
                                        value: "herceg-novi"
                                    }
                                ]
                            }
                            name={"town"}
                            control={control}
                            classNames={"w-full flex flex-col gap-3 mt-4"}
                        />
                        <TextArea
                            registration={register("description")}
                            error={errors?.description?.message ? true : false}
                            labelName={"Dodatni opis"}
                            rows={5}
                        />
                        <div className={"text-black w-full text-center"}>
                            <div className={"flex justify-center items-center gap-2"}>
                                  <span className={"font-bold text-black text-[28px]"}>
                                    €
                                </span>
                                <span className={"font-bold text-black text-[30px]"}>
                                    {handleCalculate()}
                                </span>
                            </div>
                            <span className={"text-[28px] text-black"}>
                                procena cijene:
                            </span>
                        </div>
                    </div>
                </div>
                <div className={"w-full"}>
                    <div className={"flex flex-col justify-start items-start gap-5 pt-[20px] lg:w-[500px] m-auto mt-[10px] lg:mt-[0px]"}>
                        <h1 className={"text-[25px] text-black"}>
                            Kontakt podaci
                        </h1>
                        <form className={"w-full"} onSubmit={handleSubmit(onSubmit)}>
                            <InputField
                                labelName={"Ime i prezime"}
                                registration={register("name")}
                                inputType="text"
                                classNameContainer={"w-full"}
                                error={errors?.name?.message || null}
                            />
                            <InputField
                                labelName={"Email"}
                                registration={register("email")}
                                inputType="text"
                            />
                            <InputField
                                labelName={"Br. telefona"}
                                registration={register("phone")}
                                inputType="text"
                                error={errors?.phone?.message || null}
                            />
                            <InputField
                                labelName={"Adresa radova"}
                                registration={register("address")}
                                inputType="text"
                                error={errors?.address?.message || null}
                            />
                            <div className={"flex flex-col gap-1 text-black mt-[30px] lg:w-[250px] text-center"}>
                                <Button className={"!px-[2rem] !py-[8px] !bg-[#74493d]"} isLoading={loading}>
                                    Pošalji
                                </Button>
                                Neobvezujući zahtjev
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div className={"bg-[#74493d] w-full py-[15px]"}>
                <div className={""}>
                    <div className={"container m-auto p-[24px] flex  flex-col justify-center items-center gap-5"}>
                        <h1 className={"text-[25px] text-white"}>
                            Zašto odabrati {" "}
                            <span className={"text-[28px] font-[600] text-white"}>
                                malterisanje.me
                            </span>
                        </h1>
                        <div className={"flex-col flex lg:flex-row lg:justify-center justify-start lg:items-center items-start gap-5 text-white text-[18px] w-full"}>
                          <span className={"lg:border-r lg:border-l-0 lg:pr-4 pl-4 border-l border-[#ffffff33]"}>
                              Garantirana cijena
                          </span>
                            <span className={"lg:border-r lg:pr-4 lg:border-l-0 pl-4 border-l border-[#ffffff33]"}>
                            Provjereni izvođači
                          </span>
                            <span className={"lg:border-r lg:pr-4  lg:border-l-0 pl-4 border-l border-[#ffffff33]"}>
                             Datum po izboru
                          </span>
                            <span className={"lg:border-0 lg:pr-4 pl-4 border-l border-[#ffffff33]"}>
                              Ušteda vremena i živaca
                          </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
  );
}
