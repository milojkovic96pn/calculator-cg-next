import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import {Footer} from "@/app/components/Footer";
import {Navigation} from "@/app/components/Navigation";
import {Toast} from "next/dist/client/components/react-dev-overlay/internal/components/Toast";
import {Toaster} from "react-hot-toast";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "malterisanje.me",
  description: "Generated by create next app",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={inter.className}>
      <Navigation />
      {children}
      <Footer/>
      <Toaster
          position="top-center"
          reverseOrder={false}
      />
      </body>
    </html>
  );
}
