'use client'
import {useForm} from "react-hook-form";
import {InputField} from "@/app/components/inputs/InputField";
import {yupResolver} from "@hookform/resolvers/yup";
import * as Yup from "yup";
import {useState} from "react";
import "../components/utils/icons";
import {Button} from "@/app/components/Button";
import emailjs from "@emailjs/browser";
import toast from "react-hot-toast";
import {SelectDropdown} from "@/app/components/inputs/SelectDropdown";

export default function ForMasters() {

    const [loading,setLoading] = useState(false);
    const validation = Yup.object().shape({
        name: Yup.string().required("* obavezno polje"),
        address: Yup.string().required("* obavezno polje"),
    });
    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
        watch,
        control
    } = useForm<any>({
        resolver: yupResolver(validation),
    });

    const surfaceWatch = watch("surface")
    const townWatch = watch("town")

    const [input, setInput] = useState("");

    const handleChangeCheckbox = (field:string) => {
        if (input === field) {
            setInput("")
        } else {
            setInput(field)
        }
    }

    const handleCalculate = () => {
        const price = input === "yes" ? (townWatch === undefined || townWatch === "niksic" ? 11 : 12) : (townWatch === undefined || townWatch === "niksic" ? 6 : 7)
        return Number(surfaceWatch) * price || 0
    }

    const onSubmit = async (data:any) => {
        setLoading(true)
        const serviceId = 'service_mhygzvd'
        const templateId = 'template_5lrzqw5'
        const publicKey = 'SKBV-qp_H16amWI82'

        var templateParams = {
            name: data.name,
            email: data.email,
            phone: data.phone,
            address: data.address,
        };

        emailjs.send(serviceId, templateId, templateParams, publicKey).then(
            (response) => {
                setLoading(false)
                reset()
                toast.success("Uspešna registracija!")
            },
            (error) => {
                setLoading(false)
                toast.error("Greška. Pokušajte kasnije!")
            },
        );

    };

    return (
        <main className="min-h-screen bg-[#fff9f9]">
            <div className={"flex flex-col gap-5"}>
                <div className={"bg-[#74493d] w-full"}>
                    <div className={""}>
                        <div className={"container m-auto p-[24px] lg:flex justify-center items-center gap-5 text-center"}>
                            <h1 className={"text-[25px] text-white"}>
                                Registrujte se na malterisanje.me i postanite naš partner
                            </h1>
                        </div>
                    </div>
                </div>
                <div className={"flex justify-center items-center container m-auto px-[24px] pb-[24px]"}>
                    <div className={"w-full"}>
                        <div className={"flex flex-col justify-start items-start gap-5 pt-[20px] lg:w-[500px] m-auto"}>
                            <h1 className={"text-[25px] text-black"}>
                                Kontakt podaci
                            </h1>
                            <form className={"w-full"} onSubmit={handleSubmit(onSubmit)}>
                                <InputField
                                    labelName={"Ime i prezime"}
                                    registration={register("name")}
                                    inputType="text"
                                    classNameContainer={"w-full"}
                                    error={errors?.name?.message || null}
                                />
                                <InputField
                                    labelName={"Email"}
                                    registration={register("email")}
                                    inputType="text"
                                />
                                <InputField
                                    labelName={"Br. telefona"}
                                    registration={register("phone")}
                                    inputType="text"
                                />
                                <SelectDropdown
                                    label={"Mjesto rada"}
                                    options={
                                        [
                                            {
                                                label: "Podgorica",
                                                value: "podgorica",
                                            },
                                            {
                                                label: "Budva",
                                                value: "budva"
                                            },
                                            {
                                                label: "Kotor",
                                                value: "kotor"
                                            },
                                            {
                                                label: "Tivat",
                                                value: "tivat"
                                            },
                                            {
                                                label: "Bar",
                                                value: "bar"
                                            },
                                            {
                                                label: "Ulcinj",
                                                value: "ulcinj"
                                            },
                                            {
                                                label: "Nikšić",
                                                value: "niksic"
                                            },
                                            {
                                                label: "Herceg Novi",
                                                value: "herceg-novi"
                                            }
                                        ]
                                    }
                                    name={"address"}
                                    control={control}
                                    classNames={"w-full flex flex-col gap-3 mt-4"}
                                />
                                <div className={"flex flex-col gap-1 text-black mt-[30px] lg:w-[250px]"}>
                                    <Button className={"!px-[2rem] !py-[8px] !bg-[#74493d]"} isLoading={loading}>
                                        Registrujte se
                                    </Button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div className={"bg-[#74493d] w-full py-[15px] text-white"}>
                    <div className={""}>
                        <div className={"container m-auto p-[24px] flex  flex-col justify-center items-center gap-5"}>
                            <h1 className={"text-[25px] text-white"}>
                                Zašto raditi sa nama
                            </h1>
                            <div className={"text-white text-[20px] w-full"}>
                                <b>Štedimo Vaše vrijeme</b> - unaprijed sve provjeravamo s klijentom. Ne gubite vrijeme obilazeći klijente, prepustite to malterisanje.me kako bismo Vama obavljanje posla ucinili lakše! <br/><br/>
                                <b>Budite sam svoj šef</b> - ne postoji minimalni broj obavljenih poslova. Radite kada Vama odgovara. Ako se dogodi da nemate vremena ili volje, jednostavno nam se obratite i javićemo Vam se drugi put. Vi odlučujete koliko narudžba želite izvršiti i koliko često želite raditi.
                               <br/><br/> Bez troškova registracije ili mesečnih naknada - ne rizikujete. Naknadu plaćate samo za izvršene poslove. Više ne morate brinuti o pronalazku novih narudžbina - sve odrađujemo umjesto Vas.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    );
}
