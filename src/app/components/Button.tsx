import clsx from "clsx";
import {FunctionComponent} from "react";
import {Spinner} from "@/app/components/Spinner";

export const Button : FunctionComponent<any> = props => {

    const {  type, disabled, isLoading, onClick, className,
        children, variant, editableIcon = false } = props

    return (
        <button
            disabled={disabled || isLoading ? true : false}
            onClick={onClick || undefined}
            type={type || "submit"}
            className={clsx(
                "xl:w-auto w-full px-16 py-2.5 text-center text-white rounded-lg flex items-center justify-center font-medium bg-mainColor dark:bg-darkMainColor",
                disabled || isLoading  ? "opacity-80" : "hover:opacity-90",
                editableIcon && isLoading && "!py-[7px] !px-[13.76px]",
                className && className
            )}
        >

            {editableIcon && isLoading ? null : children}
            {isLoading &&
                <Spinner editableIcon={editableIcon} />
            }
        </button>
    );
};
