'use client'
import Link from "next/link";
import {usePathname} from "next/navigation";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import clsx from "clsx";
import React, {useEffect, useState} from "react";
export const Navigation = () => {

    const pathname = usePathname()

    const links = [
        {
            label: "Početna",
            link: "/"
        },
        {
            label: "O nama",
            link: "o-nama"
        },
        {
            label: "Za majstore",
            link: "za-majstore"
        }
    ]

    const [open,setOpen] = useState(false)

    useEffect(() => {
        setOpen(false)
    }, [pathname]);

    return (
        <div className={"w-full bg-white border-b border-[#d1d5db]"}>
            {open ?
                <div className={`bg-white w-full absolute top-0  z-[99] ${open ? "h-full transition-all" : ""}`}>
                    <FontAwesomeIcon
                        onClick={() => setOpen(!open)}
                        className={clsx(
                            "text-black text-[20px] absolute right-5 top-7",
                        )}
                        icon={["fas", "times"]}
                    />
                    <div className={"flex flex-col justify-start items-start gap-5 mt-[80px] w-full"}>
                        <ul className={"flex flex-col justify-start items-start gap-5 w-full px-[20px]"}>
                            {links.map((link, index) =>
                                <li key={index}
                                    className={`${pathname === `${pathname === "/" ? "" : "/"}${link.link}` ? "active-link" : "no-active-link"} w-full hover:cursor-pointer px-[16px] py-[4px] text-black`}>
                                    <Link href={link.link}>
                                        {link.label}
                                    </Link>
                                </li>
                            )}
                        </ul>
                    </div>
                </div> : null
            }
            <div
                className={"lg:p-[24px] p-[14px] bg-white w-full mx-auto text-black flex items-center justify-between navigation container"}>
                <div className={"flex lg:justify-center lg:items-center"}>
                    <Link href={"/"} className={"text-[28px] font-[600] text-black"}>
                        malterisanje.me
                    </Link>
                </div>
                <div className={"lg:hidden"} onClick={() => setOpen(true)}>
                    <FontAwesomeIcon
                        className={clsx(
                            "text-black text-[20px]",
                        )}
                        icon={["fas", "bars"]}
                    />
                </div>
                <div className={"lg:flex hidden justify-center items-center gap-5 mt-3 lg:mt-0"}>
                    <ul className={"flex justify-center items-center gap-5"}>
                        {links.map((link, index) =>
                            <li key={index}
                                className={`${pathname === `${pathname === "/" ? "" : "/"}${link.link}` ? "active-link" : ""} hover:cursor-pointer px-[16px] py-[4px] text-black`}>
                                <Link href={link.link}>
                                    {link.label}
                                </Link>
                            </li>
                        )}
                    </ul>
                </div>
            </div>
        </div>
    )
}