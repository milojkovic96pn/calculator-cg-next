import { library } from "@fortawesome/fontawesome-svg-core";

import {
  faBars,
  faCheck, faHamburger, faPhone, faTimes,
} from "@fortawesome/free-solid-svg-icons";

library.add(
    faCheck, faPhone,faBars, faTimes
);
