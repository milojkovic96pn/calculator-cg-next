import React, {FunctionComponent} from "react";
import styled from "styled-components";

const SpinnerContainer = styled.div<{editableIcon?:boolean}>`
  .lds-ring {
    display: inline-block;
    position: relative;
    width: 15px;
    height: 15px;
  }
  .lds-ring div {
    box-sizing: border-box;
    display: block;
    position: absolute;
    width: 15px;
    height: 15px;
    margin: ${(props) =>
    props.editableIcon ? "2px 0px" : "2px 9px"};
    border: 1px solid #fff;
    border-radius: 50%;
    animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
    border-color: ${(props) =>
    props.color ? "#5e72e4 transparent transparent transparent" : "#fff transparent transparent transparent"};
  }
  .lds-ring div:nth-child(1) {
    animation-delay: -0.45s;
  }
  .lds-ring div:nth-child(2) {
    animation-delay: -0.3s;
  }
  .lds-ring div:nth-child(3) {
    animation-delay: -0.15s;
  }
  @keyframes lds-ring {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;

type TypeProps = {
    editableIcon?: boolean;
    color?: string
}
export const Spinner : FunctionComponent<TypeProps> = props => {

    const { editableIcon, color } = props

    return (
        <SpinnerContainer editableIcon={editableIcon} color={color}>
            <div className="lds-ring">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </SpinnerContainer>
    );
};
