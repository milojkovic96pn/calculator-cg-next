import {FunctionComponent} from "react";
import clsx from "clsx";
import {InputFieldMessage} from "./InputFieldMessage";

export const TextArea : FunctionComponent<any> = props => {

    const { labelName, className, registration, error, value,
        name, hidden, defaultValue, handleChange, rows,placeholder} = props;

    const style = clsx(
        `focus:shadow-primary-outline dark:bg-slate-850 dark:placeholder:text-white/80 !text-black !p-2
               dark:text-white/80 text-sm leading-5.6 ease block w-full appearance-none rounded-[4px] border
                border-solid bg-white bg-clip-padding px-[8px] py-[2px] font-normal text-gray-700 outline-none 
                transition-all placeholder:text-gray-500 focus:outline-none
               ${!error ? "border-[#cccccc] dark:border-darkMainColor"
            : "border border-rose-500"
        }`,
        className && className
    )

    return (
        <div
            className={clsx(
                `mb-4 w-full flex flex-col gap-3`,
            )}
        >
            {labelName && (
                <label className="text-[19px] text-black">
                    {labelName}
                </label>
            )}
            <div className={"relative"}>
                <textarea
                    placeholder={placeholder?.toLocaleLowerCase()}
                    value={value && value}
                    defaultValue={defaultValue && defaultValue}
                    name={name && name}
                    hidden={hidden && hidden}
                    className={style}
                    {...registration}
                    onChange={handleChange && handleChange}
                    cols={10}
                    rows={rows ? rows : 0}
                ></textarea>
                {error && (
                    <InputFieldMessage text={"* obavezno polje"} />
                )}
            </div>
        </div>
    );
};
