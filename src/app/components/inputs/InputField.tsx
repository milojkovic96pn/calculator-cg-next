import {FunctionComponent, useState} from "react";
import clsx from "clsx";
import {InputFieldMessage} from "./InputFieldMessage";

export const InputField : FunctionComponent<any> = props => {

    const { labelName, inputType, className, numberStep = 0.01, registration,
        error, name, hidden, readOnly, placeHolder, disabled,
        value, checked, copied, classNameContainer, setValue,minStepNumber,inputDivClass,extraField,type} = props;

    const style = clsx(
        `focus:shadow-primary-outline dark:bg-slate-850 !text-black
               dark:text-white/80 text-sm leading-5.6 ease block w-full appearance-none rounded-[4px] border
                border-solid bg-white bg-clip-padding px-[8px] py-[2px] font-normal text-gray-700 outline-none 
                transition-all placeholder:text-gray-500 focus:outline-none h-[39px] 
               ${!error ? "border-[#cccccc] dark:border-darkMainColor"
            : "border border-rose-500"
        } ${copied && "mb-0"}`,
        className && className
    )

    return (
        <div
            className={clsx(
                `mb-4 flex flex-col gap-3 w-full`,
                classNameContainer && classNameContainer
            )}
        >
            {labelName && (
                <label className="text-[19px] text-black">
                    {labelName}
                </label>
            )}
            <div
                className={clsx(
                    `relative`,
                    inputDivClass && inputDivClass
                )}
            >
                <div className={"flex justify-center items-center gap-2 w-full"}>
                    <input
                        type={
                            type ? type : "text"
                        }
                        placeholder={placeHolder}
                        disabled={disabled}
                        value={value && value}
                        checked={checked}
                        name={name && name}
                        hidden={hidden && hidden}
                        onWheel={(e) => e.currentTarget.blur()}
                        className={style}
                        {...registration}
                        readOnly={readOnly}
                        {...(setValue && {
                            onChange: (e) => setValue(e.target.value)
                        })}
                        min={1}
                    />
                    {extraField ? extraField : ""}
                </div>
                {error && (
                    <InputFieldMessage text={error}/>
                )}
            </div>
        </div>
    );
};
