import React, {FunctionComponent} from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import clsx from "clsx";

type typeProps = {
    onChange: () => void
    selected: boolean
    label?: string
    containerClassName?: string
    checkboxClassName?: string
    iconClassName?: string
    tooltip?: string
    disabled?: boolean
}
export const CheckboxField : FunctionComponent<typeProps> = props => {

    const {onChange,selected,label,containerClassName,checkboxClassName,
        iconClassName,tooltip,disabled = false} = props

    return (
        <div className={clsx(
            "flex lg:justify-center lg:items-center gap-2",
            containerClassName && containerClassName
        )}
        >
            <div
                onClick={() => !disabled && onChange()}
                className={clsx(
                    "border border-gray-500 bg-white rounded-[5px] w-[30px] h-[30px] flex justify-center items-center ",
                    disabled ? "cursor-not-allowed" : "cursor-pointer",
                    checkboxClassName && checkboxClassName
                )}
            >
                {selected ?
                    <FontAwesomeIcon
                        className={clsx(
                            "text-mainColor text-[20px]",
                            iconClassName && iconClassName
                        )}
                        icon={["fas", "check"]}
                    /> : null
                }
            </div>
            {label && (
                <label className="text-[19px] text-black">
                    {label}
                </label>
            )}
        </div>
    );
};
