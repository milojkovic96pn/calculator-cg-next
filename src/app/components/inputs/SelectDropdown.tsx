import Select from "react-select";
import { Controller } from "react-hook-form";
import clsx from "clsx";
import {FunctionComponent} from "react";
import {InputFieldMessage} from "@/app/components/inputs/InputFieldMessage";

export const SelectDropdown : FunctionComponent<any> = props => {

    const styleSelectdropdown = (errorText?:boolean) => {

        return {
            placeholder: (styles: any) => ({
                ...styles,
                color: "#b3bac1",
                fontSize: "0.875rem"
            }),
            control: (styles: any) => ({
                ...styles,
                height: "39px",
                "&:hover": {
                    borderColor: "none"
                },
                boxShadow: 'none',
                borderColor:  errorText ? "#f43f5e" : "#cccccc",
            }),
            singleValue: (styles: any) => ({
                ...styles,
                fontSize: "0.875rem",
            }),
            option: (styles: any, { isSelected }: any) => ({
                ...styles,
                backgroundColor: isSelected && "#74493d",
                "&:hover": {
                    backgroundColor: isSelected ? "#74493d" : "#8080801f",
                    cursor: "pointer"
                },
                ":last-of-type": {
                    borderBottom: "none"
                },
                color: isSelected ? "white" : "#3d3d3d"
            }),
            menu: (styles: any) => ({
                ...styles,
            }),
            menuList: (styles: any) => ({
                ...styles,
                paddingTop: "0px",
                paddingBottom: "0px"
            }),
        };
    }

    const { control, name, errorText, options, label, classNames, isClearable } = props

    return (
        <div className={clsx(errorText ? "" : "mb-4", classNames && classNames)}>
            <label className="text-[19px] text-black">
                {label}
            </label>
            <Controller
                control={control}
                name={name}
                render={({field: {onChange, value, ref}}) => {
                    return (
                        <Select
                            placeholder={"select"}
                            styles={styleSelectdropdown(errorText)}
                            options={options}
                            onChange={(val) => onChange(val?.value || null)}
                            isClearable={isClearable}
                            value={options?.find((c: any) => {
                                return c.value === value;
                            })}
                            defaultValue={ {
                                label: "Podgorica",
                                value: "podgorica",
                            }}
                        />
                    );
                }}
            />
            {errorText && <InputFieldMessage/>}
        </div>
    );
};
