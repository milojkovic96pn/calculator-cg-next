import Link from "next/link";

export const Footer = () => {

    const links = [
        {
            label: "Početna",
            link: "/"
        },
        {
            label: "O nama",
            link: "o-nama"
        },
        {
            label: "Za majstore",
            link: "za-majstore"
        }
    ]

    return (
        <footer className="bg-[#222222]">
            <div className="mx-auto w-full max-w-screen-xl p-4 py-6 lg:py-8">
                <div className="md:flex md:justify-between">
                    <div className="mb-6 md:mb-0 text-center">
                        <a href="https://flowbite.com/" className="lg:flex items-center">
                           <span className={"text-[28px] font-[600] text-white"}>
                                malterisanje.me
                            </span>
                        </a>
                    </div>
                    <div className="lg:grid grid-cols-2 gap-8 sm:gap-6 sm:grid-cols-3">
                        <div>
                            <ul className="text-gray-500 dark:text-gray-400 font-medium text-center">
                                {links.map((link,index) =>
                                    <li key={index} className={`mb-2 text-white`}>
                                        <Link href={link.link}>
                                            {link.label}
                                        </Link>
                                    </li>
                                )}
                            </ul>
                        </div>
                    </div>
                </div>
                <hr className="my-6 border-[#ffffff33] sm:mx-auto lg:my-8"/>
                <div className="sm:flex sm:items-center sm:justify-between">
                  <span className="text-sm text-gray-500 sm:text-center dark:text-gray-400">© 2024 {" "}
                      © Copyright 2023 <Link href={"https://mdizajn.omegalion.rs/"}><span className={"text-[#72c55e]"}>MDizajn</span></Link> . All rights reserved.
                  </span>
                </div>
            </div>
        </footer>
    )
}